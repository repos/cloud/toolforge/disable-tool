#!/usr/bin/python3
"""
Copyright 2021 Andrew Bogott
Copyright 2021 Wikimedia Foundation, Inc.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <https://www.gnu.org/licenses/>.

This script implements several commands. Each command does part of the work to
disable and/or delete a toolforge tool. It's designed to run asyncronously
on several different servers; in some case the order of operations doesn't matter
but when order does matter flag files (*.disabled) are written to the tool
homedir to signal which phases are complete.
"""
from __future__ import annotations

import argparse
from copy import deepcopy
import configparser
import datetime
import logging
from pathlib import Path
from typing import Dict, Tuple, List, TYPE_CHECKING, Any

import pymysql
import shutil
import socket
import subprocess

import ldap
from ldap import modlist

if TYPE_CHECKING:
    from ldap.ldapobject import LDAPObject

logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s %(name)s: %(message)s",
)
LOG = logging.getLogger(__name__)

QUOTA_SUFFIX = "_disable"
TOOL_HOME_DIR = Path("/data/project/")
DISABLED_K8S_FILE = "k8s.disabled"
SERVICE_MANIFEST_FILE = "service.manifest"
SERVICE_MANIFEST_DISABLED = "service.disabledmanifest"
REPLICA_CONF = "replica.my.cnf"
CONFIG_FILE = "/etc/disable_tool.conf"


def _getLdapInfo(attr: str, conffile: str = "/etc/ldap.conf") -> str:
    f = None
    try:
        f = open(conffile)
    except IOError:
        if conffile == "/etc/ldap.conf":
            # fallback to /etc/ldap/ldap.conf, which will likely
            # have less information
            f = open("/etc/ldap/ldap.conf")

    if not f:
        raise Exception(f"LDAP config file {conffile} does not exist")

    for line in f.readlines():
        if line.strip() == "":
            continue
        if line.split()[0].lower() == attr.lower():
            return line.split(None, 1)[1].strip()
    raise Exception(
        f"LDAP config file {conffile} does not have attribute '{attr}'"
    )


def _open_ldap(
    ldapHost: str | None = None,
    binddn: str | None = None,
    bindpw: str | None = None,
) -> "LDAPObject":
    sslType = _getLdapInfo("ssl")

    if ldapHost is None:
        ldapHost = _getLdapInfo("uri")

    if binddn is None:
        binddn = _getLdapInfo("BINDDN")

    if bindpw is None:
        bindpw = _getLdapInfo("BINDPW")

    ds = ldap.initialize(ldapHost)
    ds.protocol_version = ldap.VERSION3
    if sslType == "start_tls":
        ds.start_tls_s()

    try:
        ds.simple_bind_s(binddn, bindpw)
        return ds
    except ldap.CONSTRAINT_VIOLATION as e:
        raise Exception("LDAP bind failure: Too many failed attempts.") from e
    except ldap.INVALID_DN_SYNTAX as e:
        raise Exception("LDAP bind failure: The bind DN is incorrect...") from e
    except ldap.NO_SUCH_OBJECT as e:
        raise Exception(
            "LDAP bind failure: Unable to locate the bind DN account."
        ) from e
    except ldap.UNWILLING_TO_PERFORM as e:
        raise Exception(
            "LDAP bind failure: "
            "server was unwilling to perform the action requested."
        ) from e
    except ldap.INVALID_CREDENTIALS as e:
        raise Exception("LDAP bind failure: Password incorrect.") from e


def _get_tool_record(
    ds: "LDAPObject", toolname: str, projectname: str
) -> List[Any] | None:
    tool_base_dn = "ou=servicegroups,dc=wikimedia,dc=org"
    tool_record = ds.search_s(
        tool_base_dn,
        ldap.SCOPE_ONELEVEL,
        "(&(objectClass=groupOfNames)(cn={}.{}))".format(projectname, toolname),
        ["*"],
    )
    return tool_record  # type: ignore


def _disabled_datestamps(
    ds: "LDAPObject", projectname: str
) -> Dict[str, Tuple[str, datetime.datetime]]:
    disableddict: Dict[str, Tuple[str, datetime.datetime]] = {}

    basedn = "ou=people,ou=servicegroups,dc=wikimedia,dc=org"
    policy = "cn=disabled,ou=ppolicies,dc=wikimedia,dc=org"
    disabled_tools = ds.search_s(
        basedn,
        ldap.SCOPE_ONELEVEL,
        "(&(|(pwdAccountLockedTime=*)(pwdPolicySubentry={}))(cn={}.*))".format(
            policy, projectname
        ),
        ["*", "+"],
    )

    for tool in disabled_tools:
        toolname = tool[1]["cn"][0].decode("utf8").split(".")[1]
        uid = tool[1]["uidNumber"][0].decode("utf8")
        if "pwdAccountLockedTime" in tool[1]:
            timestamp = tool[1]["pwdAccountLockedTime"][0].decode("utf8")
            if timestamp == "000001010000Z":
                # This is a special case which we'll interpret to mean
                #  'disable and archive immediately'
                expirestamp = datetime.datetime.min
            else:
                cleanstamp = timestamp.rstrip("Z")
                if "." not in cleanstamp:
                    cleanstamp = cleanstamp + ".0"
                expirestamp = datetime.datetime.strptime(
                    cleanstamp, "%Y%m%d%H%M%S.%f"
                )
        else:
            # This tool is marked as disabled but we don't have an expiration
            # date so we set the date to the far future; it will be treated as
            # disabled but never expire.
            expirestamp = datetime.datetime.max

        disableddict[toolname] = uid, expirestamp

    return disableddict


def _is_expired(datestamp: datetime.datetime, days: int) -> bool:
    elapsed = (datetime.datetime.now() - datestamp).days
    LOG.debug("Elapsed days is %s", elapsed)
    return elapsed > days


# Make sure everything is properly stopped before we start deleting stuff
def _is_ready_for_archive_and_delete(tool: str, project: str) -> bool:
    tool_home = _tool_dir(tool, project)

    if not tool_home.exists():
        # This was archived on a prior path
        return True

    # This should be
    # if not get_step_complete(tool, "kubernetes_disabled")
    # once T334629 is resolved
    disabled_k8s_file = tool_home / DISABLED_K8S_FILE
    if not disabled_k8s_file.is_file():
        return False

    return True


def _current_project() -> str:
    return Path("/etc/wmcs-project").read_text().strip()


def remove_service_manifest(tool: str) -> None:
    """Remove service.manifest so that webservicemonitor leaves this tool
    alone.
    """
    manifest_file = TOOL_HOME_DIR / tool / SERVICE_MANIFEST_FILE
    disabled_manifest_file = TOOL_HOME_DIR / tool / SERVICE_MANIFEST_DISABLED
    if manifest_file.exists():
        manifest_file.rename(disabled_manifest_file)


def restore_service_manifest(tool: str) -> None:
    """Replace service.manifest"""
    manifest_file = TOOL_HOME_DIR / tool / SERVICE_MANIFEST_FILE
    disabled_manifest_file = TOOL_HOME_DIR / tool / SERVICE_MANIFEST_DISABLED
    if disabled_manifest_file.exists() and not manifest_file.exists():
        disabled_manifest_file.rename(manifest_file)


def _delete_ldap_entries(tool: str, project: str) -> None:
    """Delete all ldap references to the specified tool."""

    conf = _get_config()

    if not get_step_complete(tool, "home_archived"):
        LOG.info(
            "Tool %s is expired but home dir not yet archived. Postponing ldap deletion.",
            tool,
        )
        return

    # Get a special ldap session with read/write permissions
    novaadmin_ds = _open_ldap(
        conf["deleteldap"]["ldap_uri"],
        conf["deleteldap"]["ldap_bind_dn"],
        conf["deleteldap"]["ldap_bind_pass"],
    )

    # Doublecheck that our creds are working and we should really delete this
    disabled_tools = _disabled_datestamps(novaadmin_ds, project)
    if tool not in disabled_tools:
        LOG.warning(
            "Asked to delete %s but can't confirm that it's disabled.", tool
        )
        return

    tool_dn = "cn=%s.%s,ou=servicegroups,dc=wikimedia,dc=org" % (project, tool)
    tool_user_dn = (
        "uid=%s.%s,ou=people,ou=servicegroups,dc=wikimedia,dc=org"
        % (
            project,
            tool,
        )
    )

    # First, remove references to this tool_user_dn in other tools
    tool_base_dn = "ou=servicegroups,dc=wikimedia,dc=org"
    LOG.info("Removing ldap references to %s", tool_user_dn)
    all_tools = novaadmin_ds.search_s(
        tool_base_dn, ldap.SCOPE_ONELEVEL, "(objectClass=groupOfNames)", ["*"]
    )
    for thistool in all_tools:
        if tool_user_dn.encode("utf8") in thistool[1]["member"]:
            toremove = tool_user_dn.encode("utf8")
            new = deepcopy(thistool[1])
            new["member"].remove(toremove)
            ldif = modlist.modifyModlist(thistool[1], new)
            novaadmin_ds.modify_s(thistool[0], ldif)

    # Now remove the tool itself
    LOG.info("Removing ldap entry for %s", tool_user_dn)
    novaadmin_ds.delete_s(tool_user_dn)
    LOG.info("Removing ldap entry for %s", tool_dn)
    novaadmin_ds.delete_s(tool_dn)
    novaadmin_ds.unbind()
    set_step_complete(tool, "ldap_deleted")


config = None


def _get_config() -> configparser.ConfigParser:
    global config
    if not config:
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
    return config


def _tool_dir(tool: str, project: str) -> Path:
    conf = _get_config()
    base_dir = Path(conf["archive"][f"base_dir_{project}"])
    return base_dir / tool


def _tool_archive_file(tool: str, project: str) -> Path:
    conf = _get_config()
    archive_dir = Path(conf["archive"][f"archive_dir_{project}"])
    return archive_dir / f"{tool}.tgz"


def _archive_home(tool: str, project: str) -> None:
    """Make a tarball of the tool's project dir and delete the project dir."""
    tool_dir = _tool_dir(tool, project)

    if not _is_ready_for_archive_and_delete(tool, project):
        LOG.info(
            "Tool %s is expired but not properly shut down yet, skipping file archive",
            tool,
        )
        return

    if not tool_dir.exists():
        LOG.info("Nothing to archive in %s", tool_dir)
        # nothing to archive
        return

    archivefile = _tool_archive_file(tool, project)
    LOG.info("Archiving tool %s to %s", tool_dir, archivefile)

    args = ["tar", "-cpzf", str(archivefile), str(tool_dir)]
    rval = subprocess.call(args)
    if rval:
        LOG.info(
            "Failed to archive %s with exit code %s. Command was: %s",
            tool,
            rval,
            " ".join(args),
        )
        return

    LOG.info("Archived %s to %s; removing original", tool_dir, archivefile)

    # We need to do some special magic to get replica.my.cnf out of the way;
    #  otherwise the rmtree below will fail.
    db_conf = tool_dir / REPLICA_CONF
    if db_conf.is_file():
        subprocess.check_output(["chattr", "-i", str(db_conf)])

    shutil.rmtree(tool_dir)
    LOG.info("Removed %s", tool_dir)

    set_step_complete(tool, "home_archived")


def archive() -> None:
    conf = _get_config()
    if conf["archive"]["hostname_substring"] not in socket.gethostname():
        LOG.error("This command can only be run on the toolforge nfs server")
        exit(4)

    ds = _open_ldap()
    project = _current_project()
    disabled_tools = _disabled_datestamps(ds, project)
    for tool in disabled_tools:
        _uid, datestamp = disabled_tools[tool]
        if _is_expired(datestamp, int(conf["default"]["archive_after_days"])):
            if not _is_ready_for_archive_and_delete(tool, project):
                LOG.info(
                    "Tool %s is expired but not shut down yet. Postponing archive step.",
                    tool,
                )
                continue
            else:
                LOG.info("Tool %s is expired; archiving", tool)

            _archive_dbs(tool, project)
            _archive_home(tool, project)


def deleteldap() -> None:
    conf = _get_config()
    if conf["deleteldap"]["hostname_substring"] not in socket.gethostname():
        LOG.error("This command can only be run on a cloudcontrol server")
        exit(4)

    ds = _open_ldap()
    for project in [
        project.strip()
        for project in conf["deleteldap"]["all_projects_on_server"].split(",")
    ]:
        disabled_tools = _disabled_datestamps(ds, project)
        for tool in disabled_tools:
            _uid, datestamp = disabled_tools[tool]
            if _is_expired(
                datestamp, int(conf["default"]["archive_after_days"])
            ):
                if not get_step_complete(tool, "home_archived"):
                    LOG.info(
                        "Tool %s is expired but home dir not yet archived. Postponing ldap deletion.",
                        tool,
                    )
                    continue
                LOG.info(
                    "Tool %s is expired and archived; deleting ldap records",
                    tool,
                )
                _delete_ldap_entries(tool, project)


def set_step_complete(tool: str, step: str, state: bool = True) -> None:
    conf = _get_config()

    if state:
        stateval = 1
    else:
        stateval = 0

    connection = pymysql.connect(
        host=conf["database"]["db_host"],
        user=conf["database"]["db_username"],
        password=conf["database"]["db_password"],
        database=conf["database"]["db_name"],
    )
    query = (
        f"INSERT INTO toolstate (toolname, `{step}`) "
        "VALUES (%s, %s) "
        f"ON DUPLICATE KEY UPDATE `{step}` = %s;"
    )

    with connection.cursor() as cursor:
        cursor.execute(query, (tool, stateval, stateval))

    connection.commit()
    connection.close()


def get_step_complete(tool: str, step: str) -> bool:
    conf = _get_config()
    connection = pymysql.connect(
        host=conf["database"]["db_host"],
        user=conf["database"]["db_username"],
        password=conf["database"]["db_password"],
        database=conf["database"]["db_name"],
    )
    query = f"select `{step}` from toolstate where toolname = %s"
    with connection.cursor() as cursor:
        cursor.execute(query, (tool,))
        rows = cursor.fetchall()
    connection.close()

    if rows and rows[0][0]:
        return True

    return False


def _archive_dbs(tool: str, project: str) -> None:
    LOG.info("Archiving databases for %s", tool)

    tool_dir = _tool_dir(tool, project)
    db_conf = tool_dir / REPLICA_CONF

    if not db_conf.is_file():
        # No replica.my.cnf so nothing to do
        LOG.info(
            "No database config (%s) for %s so nothing databases to delete",
            db_conf,
            tool,
        )
        return

    dbconfig = configparser.ConfigParser()
    dbconfig.read(db_conf)

    connection = pymysql.connect(
        host="tools.db.svc.wikimedia.cloud",
        read_default_file=db_conf,
    )
    with connection.cursor() as cursor:
        cursor.execute(
            "SHOW databases LIKE '%s__%%';"
            % dbconfig["client"]["user"].strip("'")
        )
        dbs = [db[0] for db in cursor.fetchall()]
    connection.close()

    for db in dbs:
        dump_file = tool_dir / f"{db}.mysql"
        LOG.info("Archiving databases %s for %s to %s", db, tool, dump_file)
        LOG.info("Dumping %s to %s", db, dump_file)
        with dump_file.open("w") as f:
            args = [
                "mariadb-dump",
                f"--defaults-file={db_conf}",
                "-h",
                "tools.db.svc.wikimedia.cloud",
                "--quick",
                "--max_allowed_packet=1G",
                db,
            ]
            rval = subprocess.call(args, stdout=f)
            if rval != 0:
                LOG.error("Failed to dump db %s for tool %s", db, tool)
                # Something went wrong; that probably means the table
                # is undumpable We're going to be bold and just drop
                # it.

        LOG.info("Dropping db %s for tool %s", db, tool)
        connection = pymysql.connect(
            host="tools.db.svc.wikimedia.cloud",
            read_default_file=db_conf,
        )
        with connection.cursor() as cursor:
            # This looks a bit unsafe.. but it's executed as the credentials of the tool
            cursor.execute(f"DROP database `{db}`;")
        connection.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "disable-tools",
        description="Disable tools that have "
        "pwdAccountLockedTime set in ldap."
        "This needs to be run on multiple hosts, "
        "in the appropriate mode on each host.",
    )

    config = _get_config()

    sp = parser.add_subparsers()

    sp_archive = sp.add_parser(
        "archive",
        help="Archive the home dir for all tools disabled for more than %s days"
        % config["default"]["archive_after_days"],
    )
    sp_archive.set_defaults(func=archive)

    sp_deleteldap = sp.add_parser(
        "deleteldap",
        help="Delete ldap records for a tool that's"
        "been disabled for more than %s days"
        % config["default"]["archive_after_days"],
    )
    sp_deleteldap.set_defaults(func=deleteldap)

    args = parser.parse_args()

    if "func" in args:
        args.func()
    else:
        parser.print_help()
